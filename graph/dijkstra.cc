int mxi = (1 << 25);

bool cmp(pair<int, int> a, pair<int, int> b)
{
    return (a.second > b.second);
}

int dijkstra(vector<vector<pair<int, int>>> &g, int N)
{
    priority_queue<pair<int, int>, vector<pair<int, int>>, decltype(cmp) *> pq(cmp);
    vector<int> dist (N, mxi);
    dist[0] = 0;
    pq.push({0, 0});
    while(!pq.empty()) {
	int u = pq.top().first;
	int d = pq.top().second;
	pq.pop();
	if(d > dist[u]) continue;
	if(u == N-1) return d;
	for(auto it = g[u].begin(); it != g[u].end(); ++it) {
	    int v = it -> first;
	    int w = it -> second;
	    if(w + dist[u] < dist[v]) {
		dist[v] = w + dist[u];
		pq.push({v, dist[v]});
	    }
	}
    }
    return dist[N-1];
}
